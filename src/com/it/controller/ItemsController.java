package com.it.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.management.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.it.pojo.Items;
import com.it.pojo.QueryVo;
import com.it.service.ItemService;


@Controller
public class ItemsController {

	@Autowired
	private ItemService itemService ;
	
	@RequestMapping("/list")
	public ModelAndView showList(HttpServletRequest request, HttpServletResponse response) throws Exception{
		List<Items> list = itemService.findAll();
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("itemList", list);
		modelAndView.setViewName("itemList");
		return modelAndView;
	}
	//到修改界面去
	///itemEdit.action?id=${item.id}
	@RequestMapping("/itemEdit")              
	public ModelAndView itemEdit(@RequestParam(value="id",required=false,defaultValue="1")int itemId) {
		Items item = itemService.findById(itemId);
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.addObject("item", item);
		modelAndView.setViewName("editItem");
		return modelAndView;
		
	}
	
	//	商品修改
	@RequestMapping("/updateitem")
	public String updateitem(MultipartFile pictureFile,Items item,Model model) throws IllegalStateException, IOException{
		
		String originalFilename = pictureFile.getOriginalFilename();//获取上传文件的完整名称（带扩展名）
		String fileName = UUID.randomUUID().toString(); //创建一个随机数，作为即将保存图片的名字
		String ext = originalFilename.substring(originalFilename.lastIndexOf(".")); //获取文件的扩展名
		pictureFile.transferTo(new File("E:\\file\\"+fileName+ext));  //文件的保存
		item.setPic(fileName+ext);  //文件名称保持到表中
		itemService.update(item);
		return "redirect:list.action";
	}
}
