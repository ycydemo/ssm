package com.it.service;

import java.util.List;

import com.it.pojo.Items;


public interface ItemService {

	public List<Items> findAll();

	public Items findById(int itemId);

	public void update(Items item);
	
}
