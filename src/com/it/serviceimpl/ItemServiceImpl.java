package com.it.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.it.mapper.ItemsMapper;
import com.it.pojo.Items;
import com.it.pojo.ItemsExample;
import com.it.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemsMapper itemsMapper;
	public List<Items> findAll() {
		return itemsMapper.selectByExampleWithBLOBs(new ItemsExample());
	}
	public Items findById(int itemId) {
		return itemsMapper.selectByPrimaryKey(itemId);
	}
	public void update(Items item) {
		itemsMapper.updateByPrimaryKeySelective(item);
	}

}
